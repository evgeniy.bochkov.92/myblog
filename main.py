from models import (
    create_tables,
    Session,
    User,
    Post,
    Tag,
    UserProfile
)


def create_users():
    session = Session()

    admin_profile = UserProfile(
        first_name="Админ",
        last_name="Админов"
    )

    eugene_profile = UserProfile(
        first_name="Евген",
        last_name="Евгенов"
    )

    admin = User(
        name="admin",
        profile=admin_profile
    )
    eugene = User(
        name="Eugene22",
        profile=eugene_profile
    )

    session.add_all([admin, eugene])
    session.commit()
    session.close()


def create_posts():
    session = Session()

    admin = session.query(User).filter_by(name="admin").one()
    eugene = session.query(User).filter_by(name="Eugene22").one()

    tag_1 = Tag(name="пение")
    tag_2 = Tag(name="фрукты")
    tag_3 = Tag(name="ягоды")
    tag_4 = Tag(name="нимфа")

    post_1 = Post(
        title="Как поют кореллы",
        author=admin,
        tags=[tag_1, tag_4]
    )
    post_2 = Post(
        title="Чем кормить кореллу",
        author=eugene,
        tags=[tag_2, tag_3, tag_4]
    )
    post_3 = Post(
        title="Игрушки",
        author=eugene,
        tags=[]
    )

    session.add_all([post_1, post_2, post_3])
    session.commit()
    session.close()


if __name__ == '__main__':
    create_tables()
    create_users()
    create_posts()
