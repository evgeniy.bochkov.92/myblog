from models import (
    Session,
    Post,
    Tag,
    User,
    UserProfile
)
from sqlalchemy import or_, distinct


def test_get_admin_posts_by_tags():
    """
    Выбрать посты админа с тегами:
    'фрукты' и 'пение'.
    """
    posts = Session.query(Post.title). \
        join(User).join(Post.tags). \
        filter(User.name == "admin"). \
        filter(
        or_(Tag.name == "фрукты", Tag.name == "пение")).all()

    expected = ["Как поют кореллы"]

    assert len(posts) == len(expected)
    assert all([a.title == b for a, b in zip(posts, expected)])


def test_get_eugene_posts_by_tags():
    """
    Выбрать посты Евгена с тегами:
    'фрукты' и 'пение'.
    """
    posts = Session.query(Post.title). \
        join(User).join(Post.tags). \
        filter(User.name == "Eugene22"). \
        filter(
        or_(Tag.name == "фрукты", Tag.name == "пение")).all()

    expected = ["Чем кормить кореллу"]

    assert len(posts) == len(expected)
    assert all([a.title == b for a, b in zip(posts, expected)])


def test_get_users_by_tags():
    """
    Выбрать имена пользователей, разместивших посты с тегами:
    'фрукты' и 'пение'.
    """
    users = Session.query(UserProfile.first_name). \
        join(User).join(Post).join(Post.tags). \
        filter(
        or_(Tag.name == "фрукты", Tag.name == "пение")). \
        order_by(UserProfile.first_name.asc()).all()

    expected = ["Админ", "Евген"]

    assert len(users) == len(expected)
    assert all([a.first_name == b for a, b in zip(users, expected)])

    # pytest в случае ошибки не показывает где именно различия
    # возможно лучше проверять так:
    # assert users[0].name == "admin2"


def test_get_users_without_tags():
    """
    Выбрать имена пользователей, разместивших посты БЕЗ тегов.
    """
    users = Session.query(UserProfile.first_name). \
        join(User).join(Post).outerjoin(Post.tags). \
        filter(Tag.name == None).all()

    expected = ["Евген"]

    assert len(users) == len(expected)
    assert all([a.first_name == b for a, b in zip(users, expected)])
