from sqlalchemy import (
    Column,
    Integer,
    String,
    DateTime,
    func,
    ForeignKey,
    create_engine,
    Table
)
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, sessionmaker, scoped_session
from datetime import datetime

engine = create_engine("sqlite:///myblog.db", echo=True)
Base = declarative_base(bind=engine)

session_factory = sessionmaker(bind=engine)
Session = scoped_session(session_factory)

post_tag = Table(
    "post_tag", Base.metadata,
    Column("post_id", Integer, ForeignKey("posts.id")),
    Column("tag_id", Integer, ForeignKey("tags.id"))
)


class User(Base):
    __tablename__ = "users"
    id = Column(Integer, primary_key=True)
    name = Column(String(32), unique=True, default="", server_default="")
    created_at = Column(DateTime, nullable=False, default=datetime.utcnow, server_default=func.now())

    posts = relationship("Post", back_populates="author")
    profile = relationship("UserProfile", back_populates="user", uselist=False)


class UserProfile(Base):
    __tablename__ = "user_profiles"
    id = Column(Integer, primary_key=True)
    first_name = Column(String(32), nullable=False, default="", server_default="")
    last_name = Column(String(32), nullable=False, default="", server_default="")
    user_id = Column(Integer, ForeignKey("users.id"), nullable=False, unique=True)

    user = relationship("User", back_populates="profile")


class Post(Base):
    __tablename__ = "posts"
    id = Column(Integer, primary_key=True)
    title = Column(String(70), nullable=False, default="", server_default="")
    created_at = Column(DateTime, nullable=False, default=datetime.utcnow, server_default=func.now())
    user_id = Column(Integer, ForeignKey("users.id"), nullable=False)

    tags = relationship(
        "Tag",
        secondary=post_tag,
        back_populates="posts"
    )
    author = relationship("User", back_populates="posts")

    def __repr__(self):
        return f"{self.title}"


class Tag(Base):
    __tablename__ = "tags"
    id = Column(Integer, primary_key=True)
    name = Column(String(32), unique=True, default="", server_default="")

    posts = relationship(
        "Post",
        secondary=post_tag,
        back_populates="tags"
    )


def create_tables():
    Base.metadata.create_all()
